package com.usereserva.api.repository;

import java.util.List;

import com.usereserva.api.entity.Person;
import com.usereserva.api.entity.inputs.PersonInput;

public interface PersonRepositoryCustom {

	public List<Person> filter(PersonInput input);
}
