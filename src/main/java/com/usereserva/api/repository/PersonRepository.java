package com.usereserva.api.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.usereserva.api.entity.Person;

@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person, Long>{

	List<Person> findByName(String name);
	
}
