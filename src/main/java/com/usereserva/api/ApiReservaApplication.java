package com.usereserva.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiReservaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiReservaApplication.class, args);
	}

}
