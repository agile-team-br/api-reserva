
# API Reserva (CRUD Person)
_Prerequisitos:_
* docker version 1.10.3
* JDK8
* Maven

## Instalação

1. Seguindo a ordem de instalação você precisará do docker instalado em sua máquina para gerar a imagem da aplicação com o seguinte comando na raiz do projeto:

   `mvn clean package docker:build -Dmaven.test.skip=true`
   
2. Uma vez a imagem criada você poderá rodar o container da aplicação com o seguinte comando:

   `docker run -p 8080:8080 api_reserva_web`

3. Uma vez o container levantado você vai poder acessar e testar pelas urls:

- `Entrypoint Rest >> http://localhost:8080/api-reserva/persons` 
- `Entrypoint API Graphiql >> http://localhost:8080/graphiql` 
- `Entrypoint Swagger >> http://localhost:8080/swagger-ui.html#` 
 

## Testando a API via Graphiql ou Postman:

## GraphQL 

### CREATE

> Criando uma pessoa, observe que o GraphQL atende a quantidade de campos que deseja como retorno ou inclusão.
```
mutation {
    createPerson(input: {
                   name: "Alaor", 
                   lastName: "Leite", 
                   birthDate: "2012-04-21T18:25:43-05:00",
                   contacts: [ 
                                 { type: "OFFICE", number: "(21) 0533-2258" }, 
                                 { type: "HOUSE", number: "(22) 3454-3335" }
                             ]}) 
  {
    id
    name
    lastName
    birthDate
    contacts {
      type
      number
    }
  }
}
```

### UPDATE

> Atualizando uma pessoa, observe que o GraphQL atende a quantidade de campos que deseja como retorno ou inclusão.
```
mutation {
  updatePerson(input: {
                 id: 1, 
                 name: "Marcos",  
                 lastName: "Leite de Souza", 
                 birthDate: "2012-04-21T18:25:43-05:00", 
                 contacts: [ 
                               { type: "OFFICE", number: "(21) 0533-1111" }
                           ]}) 
  {
    id
    name
    lastName
    birthDate
    contacts {
      type
      number
    }
  }
}
```

### DELETE

> Deletando uma pessoa,  retornará um boolean e status 200.
```
mutation {
	deletePerson(id: 1)
}
```

### GET / Filtro pessoas

> Esse entrypoint não só faz a consulta por uma pessoa, mas filtra  por outros campos.  Pelo tempo só fiz consulta por id, name, lastName.  Mas a ideías é que tenha um único entrypoint para qualquer tipo de consulta.
```
{
  persons(input: {name: "João"}) 
  {
    id
    name
    lastName
    birthDate
    contacts {
      type
      number
    }
  }
}

Outro exemplo por idPessoa:

{
  persons(input: {id: 2}) 
  {
    id
    name
    lastName
    birthDate
  }
}

```
## Features

- `Springboot` 
- `Swagger` 
- `GraphQL` 
- `H2` 

## Débitos Técnicos

- `MongoDB` 
- `Paginação na Contulta + inclusão de alguns campos` 
- `Serviço na nuvem` 
- `Teste Integração e aceitação` 
- `CI` 
- `Camada exception` 




